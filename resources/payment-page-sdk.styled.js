(function () {
  'use strict';

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }

  function _inheritsLoose(subClass, superClass) {
    subClass.prototype = Object.create(superClass.prototype);
    subClass.prototype.constructor = subClass;
    subClass.__proto__ = superClass;
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  function _construct(Parent, args, Class) {
    if (isNativeReflectConstruct()) {
      _construct = Reflect.construct;
    } else {
      _construct = function _construct(Parent, args, Class) {
        var a = [null];
        a.push.apply(a, args);
        var Constructor = Function.bind.apply(Parent, a);
        var instance = new Constructor();
        if (Class) _setPrototypeOf(instance, Class.prototype);
        return instance;
      };
    }

    return _construct.apply(null, arguments);
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  var getClassNames = function getClassNames(element) {
    var classList = element.getAttribute('class') || '';
    return classList.split(' ').filter(Boolean);
  };

  var addClass = function addClass(element, targetClass) {
    var classNames = getClassNames(element);

    if (classNames.indexOf(targetClass) !== -1) {
      return;
    }

    classNames.push(targetClass);
    element.setAttribute('class', classNames.join(' '));
  };
  var removeClass = function removeClass(element, targetClass) {
    var classNames = getClassNames(element);

    if (classNames.indexOf(targetClass) === -1) {
      return;
    }

    classNames.splice(classNames.indexOf(targetClass), 1);
    element.setAttribute('class', classNames.join(' '));
  };

  var BaseComponent =
  /*#__PURE__*/
  function () {
    function BaseComponent() {}

    var _proto = BaseComponent.prototype;

    _proto.execute = function execute(props) {
      if (props === void 0) {
        props = {};
      }

      this.props = props;
      var elem = this.render();
      this.mount = elem;
      return this.mount;
    };

    _proto.unmount = function unmount() {
      if (!this.isMount()) {
        return;
      }

      this.mount.parentNode.removeChild(this.mount);
      this.mount = null;
    };

    _proto.isMount = function isMount() {
      return this.mount instanceof HTMLElement;
    };

    return BaseComponent;
  }();

  function styleInject(css, ref) {
    if ( ref === void 0 ) ref = {};
    var insertAt = ref.insertAt;

    if (!css || typeof document === 'undefined') { return; }

    var head = document.head || document.getElementsByTagName('head')[0];
    var style = document.createElement('style');
    style.type = 'text/css';

    if (insertAt === 'top') {
      if (head.firstChild) {
        head.insertBefore(style, head.firstChild);
      } else {
        head.appendChild(style);
      }
    } else {
      head.appendChild(style);
    }

    if (style.styleSheet) {
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
  }

  var css = ".style_root__k59xi {\n    position: fixed;\n    left: 0;\n    right: 0;\n    top: 0;\n    bottom: 0;\n    background-color: rgba(0, 0, 0, .7);\n    z-index: 100;\n}\n";
  var style = {"root":"style_root__k59xi"};
  styleInject(css);

  var Paranja =
  /*#__PURE__*/
  function (_Component) {
    _inheritsLoose(Paranja, _Component);

    function Paranja() {
      return _Component.apply(this, arguments) || this;
    }

    var _proto = Paranja.prototype;

    _proto.render = function render() {
      var _this$props = this.props,
          children = _this$props.children,
          onClick = _this$props.onClick;
      var elem = document.createElement('div');
      addClass(elem, style.root);
      elem.addEventListener('click', onClick);
      elem.appendChild(children);
      return elem;
    };

    return Paranja;
  }(BaseComponent);

  var css$1 = ".style_cover__352rz {\n    position: absolute;\n    padding: 50px;\n    left: 0;\n    top: 0;\n    right: 0;\n    bottom: 0;\n}\n\n.style_wrap__r6QoJ {\n    position: relative;\n    max-width: 700px;\n    height: 100%;\n    margin: 0 auto;\n}\n\n.style_cross__Dq62T {\n    position: absolute;\n    font-size: 1.5rem;\n    right: -27px;\n    top: -30px;\n    color: rgb(180, 180, 180);\n    cursor: pointer;\n    font-family: \"Arial\", sans-serif;\n    z-index: 101;\n}\n\n.style_inner__1WMoI {\n    width: 100%;\n    height: 100%;\n    position: relative;\n    border-radius: 5px;\n    overflow: hidden;\n    background-color: white;\n    -webkit-overflow-scrolling: touch;\n    overflow: auto;\n}\n\n.style_iframe__2XtWY {\n    width: 100%;\n    height: 100%;\n    border: none;\n}\n\n@media (max-width: 450px) {\n    .style_cross__Dq62T {\n        right: 17px;\n        top: 13px;\n        color: rgb(102, 102, 102);\n    }\n\n    .style_cover__352rz {\n        padding: 0;\n    }\n}\n";
  var style$1 = {"cover":"style_cover__352rz","wrap":"style_wrap__r6QoJ","cross":"style_cross__Dq62T","inner":"style_inner__1WMoI","iframe":"style_iframe__2XtWY"};
  styleInject(css$1);

  var PaymentPage =
  /*#__PURE__*/
  function (_Component) {
    _inheritsLoose(PaymentPage, _Component);

    function PaymentPage() {
      var _this;

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _Component.call.apply(_Component, [this].concat(args)) || this;

      _defineProperty(_assertThisInitialized(_this), "name", 'payment-page');

      return _this;
    }

    var _proto = PaymentPage.prototype;

    _proto.render = function render() {
      var onClose = this.props.onClose;
      var paranja = new Paranja();
      var cover = document.createElement('div');
      addClass(cover, style$1.cover);
      var cross = document.createElement('div');
      addClass(cross, style$1.cross);
      cross.innerText = "\u2715";
      var wrap = document.createElement('div');
      addClass(wrap, style$1.wrap);
      var inner = document.createElement('div');
      addClass(inner, style$1.inner);
      var iframe = document.createElement('iframe');
      iframe.setAttribute('name', this.name);
      addClass(iframe, style$1.iframe);
      cover.appendChild(wrap);
      wrap.appendChild(cross);
      wrap.appendChild(inner);
      inner.appendChild(iframe);
      return paranja.execute({
        children: cover,
        onClick: onClose
      });
    };

    _createClass(PaymentPage, [{
      key: "url",
      get: function get() {
        var url = this.props.url;
        var pos = url.indexOf('?');

        if (pos === -1) {
          return url;
        }

        return url.slice(0, pos);
      }
    }]);

    return PaymentPage;
  }(BaseComponent);

  var css$2 = ".style_button__1YfrP {\n    background-color: white;\n    border-color: rgba(0, 0, 0, 0.2);\n    padding: 13px 23px;\n    text-align: center;\n    border-radius: 1px;\n    outline: 0px;\n    border-width: 1px;\n    cursor: pointer;\n    font-size: 14px;\n    height: 48px;\n    display: inline-block;\n    box-sizing: border-box;\n    white-space: nowrap;\n    width: 200px;\n    font-family: Helvetica, Arial, sans-serif;\n\n    transition: background-color 0.3s ease 0s, color 0.3s ease 0s, border-color 0.3s ease 0s;\n}\n\n.style_button__1YfrP.style_active__21G-H {\n    background-color: rgb(255, 237, 0);\n    border-color: rgb(255, 237, 0);\n}\n\n.style_button__1YfrP.style_active__21G-H:hover {\n    border-color: rgb(0, 0, 0);\n    background-color: rgb(0, 0, 0);\n    color: white;\n}\n\n.style_button__1YfrP:hover {\n    border-color: rgb(0, 0, 0);\n}\n";
  var style$2 = {"button":"style_button__1YfrP","active":"style_active__21G-H"};
  styleInject(css$2);

  var Button =
  /*#__PURE__*/
  function (_Component) {
    _inheritsLoose(Button, _Component);

    function Button() {
      return _Component.apply(this, arguments) || this;
    }

    var _proto = Button.prototype;

    _proto.render = function render() {
      var _this$props = this.props,
          isActive = _this$props.isActive,
          onClick = _this$props.onClick,
          children = _this$props.children;
      var elem = document.createElement('button');
      elem.addEventListener('click', onClick);
      elem.innerText = children;
      addClass(elem, style$2.button);

      if (isActive) {
        addClass(elem, style$2.active);
      }

      return elem;
    };

    return Button;
  }(BaseComponent);

  var css$3 = ".style_confirm-panel__1s25- {\n    left: 50%;\n    top: 50%;\n    transform: translate(-50%, -50%);\n    position: absolute;\n    width: 100%;\n}\n\n.style_button-wrap__1Rcd5 {\n    text-align: center;\n}\n\n.style_button-wrap__1Rcd5 > button + button {\n    margin-left: 10px;\n}\n\n.style_label__wJvJQ {\n    color: white;\n    font-size: 18px;\n    margin-bottom: 40px;\n    text-align: center;\n    font-family: Helvetica, Arial, sans-serif;\n}\n\n@media (max-width: 450px) {\n    .style_button-wrap__1Rcd5 > button + button {\n        margin-left: 0;\n        margin-top: 10px;\n    }\n\n    .style_button-wrap__1Rcd5 {\n        margin: 0 40px;\n    }\n}\n";
  var style$3 = {"confirm-panel":"style_confirm-panel__1s25-","button-wrap":"style_button-wrap__1Rcd5","label":"style_label__wJvJQ"};
  styleInject(css$3);

  var Confirm =
  /*#__PURE__*/
  function (_Component) {
    _inheritsLoose(Confirm, _Component);

    function Confirm() {
      return _Component.apply(this, arguments) || this;
    }

    var _proto = Confirm.prototype;

    _proto.render = function render() {
      var _this$props = this.props,
          onClose = _this$props.onClose,
          onCancel = _this$props.onCancel;
      var paranja = new Paranja();
      var confirmPanel = document.createElement('div');
      addClass(confirmPanel, style$3['confirm-panel']);
      var label = document.createElement('div');
      label.innerText = 'Вы уверены, что хотите закрыть окно?';
      addClass(label, style$3.label);
      var buttonWrap = document.createElement('div');
      addClass(buttonWrap, style$3['button-wrap']);
      var closeButton = new Button();
      var cancelButton = new Button();
      confirmPanel.appendChild(label);
      confirmPanel.appendChild(buttonWrap);
      buttonWrap.appendChild(closeButton.execute({
        onClick: onClose,
        isActive: true,
        children: 'Закрыть'
      }));
      buttonWrap.appendChild(cancelButton.execute({
        onClick: onCancel,
        children: 'Отмена'
      }));
      return paranja.execute({
        children: confirmPanel
      });
    };

    return Confirm;
  }(BaseComponent);

  var classProvider = (function (TargetClass) {
    var instance = null;
    return (
      /*#__PURE__*/
      function () {
        function _class() {
          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          instance = _construct(TargetClass, args);
        }

        var _proto = _class.prototype;

        _proto.openPopup = function openPopup() {
          var _instance;

          (_instance = instance).openPopup.apply(_instance, arguments);
        };

        _proto.open = function open() {
          var _instance2;

          (_instance2 = instance).open.apply(_instance2, arguments);
        };

        return _class;
      }()
    );
  });

  var VERSION = '1.0.5';

  var SUCCESS_RESULT = 'success';
  var FAILED_RESULT = 'failed';

  var addMessageListener = function addMessageListener(elem, mapping, targetOrigin) {
    var listener = function listener(e) {
      if (!e || !e.data || e.origin !== targetOrigin) {
        return;
      }

      var data = JSON.parse(e.data);

      if (!data.event || typeof mapping[data.event] !== 'function') {
        return;
      }

      mapping[data.event](data.content);
    };

    elem.addEventListener('message', listener, false);
    return listener;
  };
  var removeMessageListener = function removeMessageListener(elem, listener) {
    elem.removeEventListener(elem, listener);
  };

  var prepareUrl = (function (url) {
    if (url[url.length - 1] !== '/') {
      return url;
    }

    return url.slice(0, url.length - 1);
  });

  var changeLocation = (function (location) {
    window.location.replace(location);
  });

  var css$4 = ".style_body-scroll-disable__2B5zr {\n    overflow: hidden;\n    position: relative;\n    height: 100%;\n}\n";
  styleInject(css$4);

  var css$5 = ".style_body-scroll-disable__4o99q {\n    overflow: hidden;\n    position: relative;\n    height: 100%;\n}";
  var style$4 = {"body-scroll-disable":"style_body-scroll-disable__4o99q"};
  styleInject(css$5);

  var disableScroll = function disableScroll() {
    addClass(document.body, style$4['body-scroll-disable']);
    var html = document.getElementsByTagName('html')[0];
    addClass(html, style$4['body-scroll-disable']);
  };
  var enableScroll = function enableScroll() {
    removeClass(document.body, style$4['body-scroll-disable']);
    var html = document.getElementsByTagName('html')[0];
    removeClass(html, style$4['body-scroll-disable']);
  };

  var prepareValue = function prepareValue(value) {
    if (value instanceof Object) {
      try {
        return JSON.stringify(value);
      } catch (e) {
        return '';
      }
    }

    return value;
  };

  var PaymentPageSdk = function PaymentPageSdk(paymentData, targetElem, url) {
    var _this = this;

    if (paymentData === void 0) {
      paymentData = {};
    }

    if (url === void 0) {
      url = 'https://e-commerce.raiffeisen.ru/pay';
    }

    _defineProperty(this, "openPopup", function () {
      if (_this.paymentPage && _this.paymentPage.isMount()) {
        return;
      }

      _this.paymentPage = new PaymentPage();

      _this.mount.appendChild(_this.paymentPage.execute({
        paymentData: _this.paymentData,
        onClose: _this.closePopup,
        url: _this.url
      }));

      _this.submitForm(_this.paymentPage.name);

      disableScroll();
    });

    _defineProperty(this, "closePopup", function () {
      if (_this.confirm && _this.confirm.isMount() || !_this.paymentPage || !_this.paymentPage.isMount()) {
        return;
      }

      _this.confirm = new Confirm();

      _this.mount.appendChild(_this.confirm.execute({
        onClose: _this.forceClosePopup,
        onCancel: function onCancel() {
          return _this.confirm.unmount();
        }
      }));
    });

    _defineProperty(this, "forceClosePopup", function () {
      if (_this.paymentPage) {
        _this.paymentPage.unmount();
      }

      if (_this.confirm) {
        _this.confirm.unmount();
      }

      removeMessageListener(window, _this.messageBinding);
      enableScroll();
    });

    _defineProperty(this, "submitForm", function (target) {
      if (target === void 0) {
        target = '_self';
      }

      var form = document.createElement('form');
      form.setAttribute('action', _this.url);
      form.setAttribute('method', 'POST');
      form.setAttribute('target', target);
      Object.keys(_this.paymentData).forEach(function (paymentDataKey) {
        var input = document.createElement('input');
        var value = prepareValue(_this.paymentData[paymentDataKey]);
        input.setAttribute('value', value);
        input.setAttribute('name', paymentDataKey);
        form.appendChild(input);
      });

      _this.mount.appendChild(form);

      form.submit();

      _this.mount.removeChild(form);
    });

    _defineProperty(this, "open", function (isTargetBlank) {
      _this.submitForm(isTargetBlank ? '_blank' : '_self');
    });

    _defineProperty(this, "handleFinishPayment", function (content) {
      if (content.result === SUCCESS_RESULT && _this.paymentData.successUrl) {
        changeLocation(_this.paymentData.successUrl);
        return;
      }

      if (content.result === FAILED_RESULT && _this.paymentData.failUrl) {
        changeLocation(_this.paymentData.failUrl);
        return;
      }

      _this.forceClosePopup();
    });

    if (targetElem instanceof HTMLElement) {
      this.mount = targetElem;
    } else {
      this.mount = document.body;
    }

    this.paymentData = _extends({}, paymentData, {
      version: VERSION
    });
    this.url = prepareUrl(url);
    this.messageBinding = addMessageListener(window, {
      finish: this.handleFinishPayment
    }, this.url);
  };

  var PaymentPageSdk$1 = classProvider(PaymentPageSdk);

  window.PaymentPageSdk = PaymentPageSdk$1;

}());
