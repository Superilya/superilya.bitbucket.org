(function() {
    document.getElementById('orderId').value = Math.floor(Math.random() * 99999).toString().substr(0, 5);

    var getPaymentData = function () {
        var extraString = document.getElementById('extra').value;
        var receiptString = document.getElementById('receipt').value;
        var result = {
            amount: document.getElementById('amount').value,
            orderId: document.getElementById('orderId').value,
            merchantUrl: document.getElementById('merchantUrl').value,
            merchantName: document.getElementById('merchantName').value,
            successUrl: document.getElementById('successUrl').value,
            failUrl: document.getElementById('failUrl').value,
            publicId: document.getElementById('publicId').value,
            comment: document.getElementById('comment').value,
        };

        try {
            var extra = JSON.parse(extraString);

            result.extra = extra;
        } catch (e) {

        }

        try {
            var receipt = JSON.parse(receiptString);

            result.receipt = receipt;
        } catch (e) {

        }

        return result;
    };

    var getTarget = function() {
        return document.getElementById('target').value
    }

    document.getElementById('openPopup').addEventListener('click', function() {
        var paymentPage = new PaymentPageSdk(getPaymentData(), null, getTarget());

        paymentPage.openPopup();
    });

    document.getElementById('openSelf').addEventListener('click', function() {
        var paymentPage = new PaymentPageSdk(getPaymentData(), null, getTarget());

        paymentPage.open();
    });

    document.getElementById('openBlank').addEventListener('click', function() {
        var paymentPage = new PaymentPageSdk(getPaymentData(), null, getTarget());

        paymentPage.open(true);
    });
})();
